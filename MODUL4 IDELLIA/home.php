<!DOCTYPE html>
<?php
session_start();
$user = $_SESSION['user'];
$id = $_SESSION['id'];
$email = $_SESSION['email'];
?>
<head>
    <title>Home | EAD Store</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <style>
        .logo {
            width: 15%;
            height: 15%;
        }
        .menubar {
            overflow: hidden;
            padding: 5px;
            background-color: white;
            position: fixed;
            top: 0;
            width: 100%;
        }
        .menu {
            margin-right: 20px;
            float: right;
            padding: 15px 15px;
            text-align: center;
        }
        .content {
            margin-top: 85px;
        }
        .welcome {
            background-image: repeating-linear-gradient(90deg, #b2e2f3, #F5FBFD, #e0f3f9, #b2e2f3);
            height: 200px;
            width: 62%;
            margin: auto;
            padding: 10px 50px;
        }
        .product {
            display: flex;
            padding: 30px;
            padding-right: 15%;
            padding-left: 15%;
            margin: auto;
        }
        .web {
            box-shadow: 1px 1px 5px grey;
            flex: 10%;
            background-color: #f2f2f2;
            height: 540px;
            width: 100px;
            float: left;
        }
        .java {
            box-shadow: 1px 1px 5px grey;
            flex: 10%;
            background-color: #f2f2f2;
            height: 540px;
            width: 100px;
            float: left;
            margin-left: 20px;
        }
        .python {
            box-shadow: 1px 1px 5px grey;
            flex: 10%;
            background-color: #f2f2f2;
            height: 540px;
            width: 100px;
            float: left;
            margin-left: 20px;
        }
        .description-container {
            box-shadow: 1px 1px 4px grey;
            background-color: white;
            width: 100%;
        }
        .description {
            font-size: 12px;
            padding: 5px;
            margin-left: 8px;
            margin-right: 8px;
        }
        .buy {
            text-align: center;
            padding: 20px;
        }
        .buybutton {
            background-color: #007bff;
            border: none;
            color: white;
            width: 90%;
            padding: 10px;
            text-align: center;
            font-size: 11pt;
            border-radius: 5px;
        }
        .footer {
            bottom: 0;
            /*position: fixed;*/
            width: 100%;
            height: 50px;
            color: black;
            background-color: #f2f2f2;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center
        }
        .login, .register {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .login-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            width: 25%;
            height: 70%;
        }
        .register-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            width: 25%;
            height: 80%;
        }
        .closebutton {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }
        .buttonsinsidemodal {
            float: right;
        }
        .closelogin, .closeregister {
            background-color: #697581;
            border: none;
            border-radius: 2px;
            color: white;
            padding: 8px 10px;
            text-align: center;
            font-family: Roboto Light;
        }
        .loginlogin, .registerregister {
            background-color: #007dfe;
            border: none;
            border-radius: 2px;
            color: white;
            padding: 8px;
            text-align: center;
            font-family: Roboto Light;
        }
    </style>
</head>

<body style="font-family: Roboto Light;">
    <div class="menubar">
        <img src="EAD.png" class="logo"/>
        <div class="menu">
        <?php
            //kalo belum login                    
            if(!isset($_SESSION['user'])){
                echo "<a href='javascript:clicklogin();'>Login</a>
                <a href='javascript:clickregister();'>Register</a>";
            }else{
            //kalo udah berhasil login
            //ini belom dibikin sih css nya, baru tulisannya aja
                echo "<a href='cart.php' style='padding: 10px;'><img src='cart.png' width='20px'></a>";
                echo "<a href='updateprofile.php' style='padding:10px;'>$user</a>"; //username si orang yang login
                echo "<a href='logout.php' style='padding:10px;'>Logout</a>";
            }
            ?>
            
        </div>
        <hr>
    </div>
    <div class="content">
    <form action="buy.php" method="GET">
        <div class="welcome">
            <p style="font-size: 30pt;">Hello Coders</p>
            <p style="font-size: 12pt;">Welcome to our store, please take a look for the products you might buy</p>
        </div>
        <div class="product">
            <div class="web">
            <img src="products_web.png" width="100%"/>
                <div class="description-container">
                    <div class="description">
                    <b>
                        <p style="font-size: 14pt;">Learn Basic Web Programming</p>
                        <p style="font-size: 12pt;">Rp 210.000,-</p>
                    </b>
                    <p>Want to be able to make a website? Learn basic components such as HTML, CSS, and JavaScript in this class curriculum.</p>
                    </div>
                </div>
                <div class="buy">
                    <input type="submit" name="buyweb" class="buybutton" value="Buy"></input>
                </div>
            </div>
            <div class="java">
                <img src="products_java_200px.png" width="100%"/>
                <div class="description-container">
                    <div class="description">
                    <b>
                        <p style="font-size: 14pt;">Starting Programming in Java</p>
                        <p style="font-size: 12pt;">Rp 150.000,-</p>
                    </b>
                    <p>Learn Java language for you who want to learn the most popular Object-Oriented Programming (OOP) concepts for developing applications.</p>
                    </div>
                </div>
                <div class="buy">
                <input type="submit" name="buyjava" class="buybutton" value="Buy"></input>
                </div>
            </div>
            <div class="python">
            <img src="products_python.png" width="100%"/>
                <div class="description-container">
                    <div class="description">
                    <b>
                        <p style="font-size: 14pt;">Starting Programming in Python</p>
                        <p style="font-size: 12pt;">Rp 200.000,-</p>
                    </b>
                    <p>Learn Python - Fundamental various current industry trends Data Science, Maching Learning, Infrastructure-management.</p>
                    </div>
                </div>
                <div class="buy">
                    <input type="submit" name="buypython" class="buybutton" value="Buy"></input>
                </div>
            </div>
        </div>
        </form>
        <div class="footer">
            <span>© EAD Store</span>
        </div>
    </div>
    <div class="login" id="login">
        <div class="login-content">
            <span class="closebutton" onclick="javascript:closelogin();">&times;</span>
            <br>
            <p><b>Login</b></p>
            <hr>
            <form action="login.php" method="POST">
                <fieldset>
                    <p>E-mail Address</p>
                    <input type="text" name="email" style="width: 100%; height: 25px;" required />
                    <br>
                    <p>Password</p>
                    <input type="password" name="password" style="width: 100%; height: 25px;" required />
                    <br> <br> <hr> <br>
                    <div class="buttonsinsidemodal">
                        <button class="closelogin" onclick="javascript:closelogin();">Close</button>
                        <input class="loginlogin" type="submit" name="submit"value="Login"></button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="register" id="register">
        <div class="register-content">
            <span class="closebutton" onclick="javascript:closeregister();">&times;</span>
            <br>
            <p><b>Register</b></p>
            <hr>
            <form action='register.php' method="POST">
                <fieldset>
                    <p>E-mail Address</p>
                    <input type="email" name="email" style="width: 100%; height: 25px;" required />
                    <br>
                    <p>Username</p>
                    <input type="text" name="username" style="width: 100%; height: 25px;" required />
                    <br>
                    <p>Mobile Number</p>
                    <input type="number" name="mobile_num" style="width: 100%; height: 25px;" required />
                    <p>Password</p>
                    <input type="password" name="password" style="width: 100%; height: 25px;" required />
                    <br>
                    <br> <br> <hr> <br>
                    <div class="buttonsinsidemodal">
                        <button class="closeregister" onclick="javascript:closeregister();">Close</button>
                        <input class="registerregister" type="submit" name="submit" value="Register">
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        function clicklogin() {
            login.style.display = "block";
            }
        function closelogin() {
            login.style.display = "none";
        }
        function clickregister() {
            register.style.display = "block";
        }
        function closeregister() {
            register.style.display = "none";
        }
    </script>
</body>