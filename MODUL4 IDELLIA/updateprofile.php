<!DOCTYPE html>
<?php
    session_start();
    $user = $_SESSION['user'];
    $email = $_SESSION['email'];
    if((isset($_SESSION["login"]))){
        echo "<script>window.location.href='home.php'</script>";
            exit;
    }
?>

<head>
    <title>Update Profile | EAD Store</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <style>
    .logo {
        width: 15%;
        height: 15%;
        }
    .menubar {
        padding: 5px;
        background-color: white;
        position: fixed;
        top: 0;
        width: 100%;
        }
    .menu {
        margin-right: 20px;
        float: right;
        padding: 15px 15px;
        text-align: center;
        }
    .content {
        margin-top: 85px;
        align: center;
    }
    .carttable {
        margin: auto;
    }
    td {
            width: 200px;
            padding: 5px;
        }
    .sendbutton {
        background-color: blue;;
            border: none;
            border-radius: 2px;
            color: white;
            padding: 10px 200px;
            text-align: center;
    }
    .cancelbutton {
            background-color: white;
            border: 1px solid blue;
            border-radius: 2px;
            color: #9b4dca;
            padding: 8px 200px;
            text-align: center;
    }
    </style>
</head>

<body style="font-family: Roboto Light;">
    <div class="menubar">
        <img src="https://i.ibb.co/tzLYt1D/EAD.png" onclick="home.php" class="logo"/>
        <div class="menu">
            <?php                    
                echo "<a href='cart.php'><img src='cart.png' width='20px'></a>";
                echo "<a> $user </a>";
                echo "<a href='logout.php'> Logout </a>";
            ?>
        </div>
        <hr>
    </div>
    <div class="content" align="center">
        <h1>Profile</h1>
        <form action='saveupdate.php' method="POST">
                <table style="text-align: left;">
                    <tr>
                    <td>Email</td>
                    <td> <?=$_SESSION['email']?> </td>
                    </tr>
                    <tr>
                    <td>Username</td>
                    <td><input type="text" name="username_update" style="width: 400px; height: 25px;"/></td>
                    </tr>
                    <tr>
                    <td>Mobile Number</td>
                    <td><input type="number" name="mobile_num_update" style="width: 400px; height: 25px;"/></td>
                    </tr>
                    <tr>
                    <td>New Password</td>
                    <td><input type="password" name="password_update" style="width: 400px; height: 25px;"/></td>
                    </tr>
                    <tr>
                    <td>Confirm Password</td>
                    <td><input type="password" name="password_confirm" style="width: 400px; height: 25px;"/></td>
                    </tr>
                   <tr>
                   <td colspan="2" style="text-align: center;"></td>
                   </tr>
                   <tr>
                   <td colspan="2" style="text-align: center;"></td>
                   </tr> 
        </table>
        <br>
        <input id="save" class="sendbutton" type="submit" value="Save">
        <br> <br> <br>
        <a href="home.php" class="cancelbutton">Cancel</a>
        </form>
    </div>
</body>

</html>
