<!DOCTYPE html>

<?php
session_start();
$user = $_SESSION['user'];
include "config.php";

?>

<head>
    <title>Cart | EAD Store</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <style>
    .logo {
        width: 15%;
        height: 15%;
        }
    .menubar {
        overflow: hidden;
        padding: 5px;
        background-color: white;
        position: fixed;
        top: 0;
        width: 100%;
        }
    .menu {
        margin-right: 20px;
        float: right;
        padding: 15px 15px;
        text-align: center;
        }
    .content {
        margin-top: 85px;
        align: center;
    }
    .carttable {
        margin: auto;
    }
    </style>
</head>

<body style="font-family: Roboto Light;">
    <div class="menubar">
        <img src="https://i.ibb.co/tzLYt1D/EAD.png" class="logo"/>
        <div class="menu">
            <?php                    
                echo "<a href='cart.php' style='padding: 10px;'><img src='cart.png' width='20px'></a>";
                echo "<a href='updateprofile.php' style='padding:10px;'>$user</a>";
                echo "<a href='logout.php' style='padding:10px;'>Logout</a>";
            ?>
        </div>
        <hr>
    </div>
    
    <div class="content" align="center">
        <div class="carttable">
            <table align="center" style="text-align: center; " class="shoppingcart">
                <tr>
                <th style="width: 50px;  height: 50px;">No</th>
                <th style="width: 400px;  height: 50px;">Product</th>
                <th style="width: 150px;  height: 50px;">Price</th>
                <th style="width: 50px;  height: 50px;">Action</th>
                </tr>

                <?php
    $id = $_SESSION["id"];
    $belanja = mysqli_query($conn, "SELECT * FROM cart WHERE user_id = '$id'");
    $a=1;
    $total=0;
        while($row = mysqli_fetch_array($belanja)){
            echo "<tr>";
            echo "<td style='height: 50px;'>$a</td>";
            echo "<td>";
            echo $row['product'];
            echo "</td>";
            echo "<td>";
            echo $row['price'];
            echo "</td>";
            echo "<td><a href='delete.php?id=".$row['id']." 'name='delete'>X</a></td>";
            $a=$a+1;
            $total=$total+$row['price'];
            echo "</tr>";
           }
           ?>
           <tr>
        <td colspan="2" style="height: 50px;"><b>Total</b></td>
        <td><b><?=$total?></b></td>
    </tr>
            </table>
        </div>
    </div>
</body>