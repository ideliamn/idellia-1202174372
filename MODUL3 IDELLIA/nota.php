<?php

$id = $_POST['nomororder'];
$nama = $_POST['namapemesan'];
$email = $_POST['email']; 
$alamat = $_POST['alamatorder'];
$pembayaran = $_POST['pembayaran'];

if (isset($_POST["minuman"])) {
    $drink = $_POST['minuman'];
    $c = count($drink);
    $harga = 0;
    for($i=0; $i<$c; $i++) {
        if ($drink[$i]==1) {
            $harga = $harga+28000;
        }
        if ($drink[$i]==2) {
            $harga = $harga+18000;
        }
        if ($drink[$i]==3) {
            $harga = $harga+15000;
        }
        if ($drink[$i]==4) {
            $harga = $harga+30000;
        }
        if ($drink[$i]==5) {
            $harga = $harga+28000;
        }
    }
    $member = $_POST["member"];
    if($member == "memberya") {
        $harga = $harga-($harga*10/100);
    }
    else {
        $harga = $harga;
    }
}
 else {
     $harga = 1200;
    }
?>

<!DOCTYPE html>
<head>
    <title>Struk Belanja</title>
    <style>
        body {
            text-align: center;
            font-family: Roboto Light;
        }
        td {
            width: 100px;
            padding: 20px;
        }
        </style>
</head>
<body>
    <h2>Transaksi <br> Pemesanan</h2>
    <p>Terima kasih telah berbelanja di Kopi Susu Duaarrr!</p>
    <h1 style="font-size:40px;">
    Rp
    <?php echo "$harga"; ?>
    .00,-</h1>
    <br>
    <table align="center" style="text-align: left; border-bottom: 1px solid #ddd;" rules="rows">
        <tr>
            <td>
                <b>ID</b>
            </td>
            <td>
                <?php echo "$id"; ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Nama</b>
            </td>
            <td>
                <?php echo "$nama"; ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Email</b>
            </td>
            <td>
                <?php echo "$email"; ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Alamat</b>
            </td>
            <td>
                <?php echo "$alamat"; ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Pembayaran</b>
            </td>
            <td>
                <?php echo "$pembayaran"; ?>
            </td>
        </tr>
    </table>
</body>