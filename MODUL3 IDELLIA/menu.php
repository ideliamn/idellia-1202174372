<?php
$nama = $_GET['namadriver'];
$telpon = $_GET['notelp'];
$tanggal = $_GET['pickdate']; 
$asal = $_GET["pesandari"];
$bawakantong = $_GET["bawakantongbelanja"] ? "Ya" : "Tidak";
?>

<!DOCTYPE html>
<head>
    <title>Menu</title>
    <style>
        .divluar {
            display: flex;
        }
        .divdalam {
            flex: 50%;
        }
        .scrollable {
            flex: 50%;
            height: 620px;
            overflow-y: scroll;
        }
        .kembalibutton {
            background-color: #9b4dca;
            border: none;
            border-radius: 2px;
            color: white;
            padding: 10px 30px;
            text-align: center;
        }
        .cetakstrukbutton {
            background-color: #9b4dca;
            border: none;
            border-radius: 2px;
            color: white;
            padding: 10px 100px;
            text-align: center;
        }
        td {
            width: 200px;
            padding: 20px;
        }
    </style>
</head>

<body style="text-align: center; font-family: Roboto Light;">
<form action="nota.php" method="POST">
    <div class="divluar">
        <div class="divdalam">
            <h2>~ Data Driver Ojol ~</h2>
            <p><b>Nama</b><p>
            <p><?= $nama ?></p>
            <p><b>Nomor Telepon</b><p>
            <p><?= $telpon ?></p>
            <p><b>Tanggal</b><p>
            <p><?= $tanggal ?></p>
            <p><b>Asal Driver</b><p>
            <p><?= $asal ?></p>
            <p><b>Bawa Kantong</b><p>
            <p><?= $bawakantong ?></p>
            <button class="kembalibutton" onclick="location.href='form.html';"> << KEMBALI </button>
        </div>
    </form>
        <div class="scrollable">
            <h2>~ Menu ~</h2>
            <p>Pilih Menu<p>
        <table align="center" style="text-align: left; border-bottom: 1px solid #ddd;" rules="rows">
            <tr>
                <td>
                <input type="checkbox" name="minuman[]" id="drink" value="1"><b>Es Coklat Susu</b>
                </td>
                <td style="width: 200px;  padding: 20px;">
                    Rp 28.000,-
                </td>
            </tr>
            <tr>
            <td>
                <input type="checkbox" name="minuman[]" id="drink" value="2"><b>Es Coklat Matcha</b>
                </td>
                <td style="width: 200px;  padding: 20px;">
                    Rp 18.000,-
                </td>
            </tr>
            <tr>
            <td>
                <input type="checkbox" name="minuman[]" id="drink" value="3"><b>Es Susu Mojicha</b>
                </td>
                <td>
                    Rp 15.000,-
                </td>
            </tr>
            <tr>
            <td>
                <input type="checkbox" name="minuman[]" id="drink" value="4"><b>Es Matcha Latte</b>
                </td>
                <td style="width: 200px;  padding: 20px;">
                    Rp 30.000,-
                </td>
            </tr>
            <tr>
            <td>
                <input type="checkbox" name="minuman[]" id="drink" value="5"><b>Es Taro Susu</b>
                </td>
                <td>
                    Rp 28.000,-
                </td>
            </tr>
            </form>
            <tr>
                <td>
                    Nomor Order
                </td>
                <td>
                    <input type="text" name="nomororder" style="width: 250px; height: 25px" required />
                </td>
            </tr>
            <tr>
                <td>
                    Nama Pemesan
                </td>
                <td>
                    <input type="text" name="namapemesan" style="width: 250px; height: 25px" required />
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    <input type="text" name="email" style="width: 250px; height: 25px" required />
                </td>
            </tr>
            <tr>
                <td>
                    Alamat Order
                </td>
                <td>
                    <input type="text" name="alamatorder" style="width: 250px; height: 25px" required />
                </td>
            </tr>
            <tr>
                <td>
                    Member
                </td>
                <td>
                    <input type="radio" name="member" id="memberyes" value="memberya" required />
                    Ya
                    <input type="radio" name="member" id="memberno" value="membertidak" required />
                    Tidak
                </td>
            </tr>
            <tr>
                <td>
                    Metode Pembayaran
                </td>
                <td>
                    <select name="pembayaran" style="font-family: Roboto Light; font-size: 16px;">
                    <option selected disabled>-- Pilih metode pembayaran --</option>
                    <option value="Cash">Cash</option>
                    <option value="E-Money (OVO/GOPAY)">E-Money (OVO/GOPAY)</option>
                    <option value="Credit Card">Credit Card</option>
                    <option value="Lainnya">Lainnya</option>
                    </select>
                </td>
            </tr>
        </table>
        <br>
        <input class="cetakstrukbutton" type="submit" value="CETAK STRUK" style>
        </div>
    </div>
    </form>
</body>