@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach ($ngulang as $entry)
            <div class="card">
                <div class="card-header">
                <!--
                <img src="{{ Auth::user()-> avatar }}" class="rounded-circle" style="width: 50px; height: 50px;">
                <b>{{ Auth::user()->name }}</b>
                </div>
                -->
                <!--
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif -->
                    <center>
                    
                    </center>
                    <!-- You are logged in! -->
                    <img src="{{asset($entry->avatar)}}" class="rounded-circle" style="width: 50px; height: 50px;">
                    <b> {{$entry->name}} </b>
                    </div>

                    <div class="card-body">
                    <img src="{{asset($entry->image)}}" class="card-img-top">
                    <b> {{$entry->name}} </b> <br>
                    {{$entry->caption}}
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                    {{ session('status')}}
                    </div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
<style>
.card{
    margin-bottom:30px;
}
</style>